# Addon - Template #

This project is a skeleton Addon designed to quickly start and publish your work.

## Features ##

 - Basic lua and xml files, ready to run
 - Setup wizard to customize the project
 - Central version number setting - no need to update multiple files
 - Automatic build process, consisting of:
	- melder_info.ini and download button generator
	- updating version + author information
	- packaging + naming a Meldii compatible zip file
	- optional duplication of the zip to a location of your choosing
	- creating a bbcoded text file from a readme
  
## Requirements ##

 - Node.js installed on your system
 - git to clone the repository (optional)
 - Basic command-line usage
  
## Setting up ##

### 1. Installing node.js ###

Download and install [node.js](https://nodejs.org/) if you haven't already.

### 2. Downloading the template  ###

#### With git ####

Open a command line window in the parent folder where you want to create your files.

Run the following command:

	git clone https://bitbucket.org/Jotaro__/template.git YourAddonName

The files should be downloaded to a folder named YourAddonName.  

#### Manual download ####

Go to [https://bitbucket.org/Jotaro__/template/downloads](https://bitbucket.org/Jotaro__/template/downloads) and click "Download repository".

Extract the contained folder and rename it to YourAddonName.

### 3. Customizing your project ###

Open a command line window if you haven't already, and navigate to your project folder.

Tips for windows users:

- use a drive letter with a colon to switch to a partition when needed
- you can copy the path from your explorer and use right click to insert it after the `cd` command

Example:

	C:\Users\Jotaro
	> d:
	D:\
	> cd D:\Jotaro\Documents\FirefallFiles\LDT\Template

Next, run

	npm install

to download any required dependencies for node and gulp.

Once this is done, run
	
	gulp setup

This will ask you a few questions about your project and set it up for you, which includes updating the configuration, editing as well as renaming the files so you have a clean and working starting point.

You're now ready to start editing your project. If you use git, it would be best to `git init` and do your initial commit now. 

## Using the automated build process ##

### 1. The package.json file ###

This file is located in the root of your project and keeps all Addon information in one central place.

It looks something like this:

	{
		"name": "Template",
		"author": "Jotaro",
		"version": "1.0.0",
		"description": "",
		"patch": "x.x.xxxx",
		"url": "",
		"destination": "gui/components/MainUI/Addons/",
	...

These options should already be filled with relevant information by the setup wizard, but you can change them to your liking.

Keep in mind that the Addon name will not be automatically updated when building, you'll need to rename the files and the corresponding lines manually.

### 2. Variables inside your files ###

Both `YourAddonName.lua` and `YourAddonName.xml` will have a few places with strings like this one: `~%version%~`

You should not touch them while developing, since they will be picked up by the build process and be replaced with the proper information from your `package.json`.

### 3. Running a build ###

Once your Addon is ready to be distributed, open a command line inside the root folder of your project and type:

	gulp

This starts the default gulp task, which will copy all your files inside the `./src` folder to a temporary `./build` folder.  
Additionally, a `melder_info.ini` will be created and placed in this folder as well.  
If you keep a `readme.md` file, it will also be copied.  
Everything will be packaged in an automatically named .zip file (like `YourAddonName_1.2.3`) and saved to a new `./dist` folder.

In addition to the zip file, a `melder_button.txt` and, if a `readme.md` is present, a bbcoded `readme.txt` will be placed inside the `./dist` folder.

The `./build` folder will be deleted, but if you prefer to keep it there is an option included to do so.

### 4. Using the readme converter ###

This project comes with a `readme.md` file located in the root folder.

This file can be used to write your Addon documentation, and is automatically converted to a bbcode text file located in `./dist/readme.txt`, ready to be posted in any supporting editor like forums or project sites.

#### Note about underline and color options ####

If you use underscores to highlight parts of your text, they will be converted to either underline (single) or color (double) tags.
HTML Markdown parsers will produce italics or bold instead.

### 5. Advanced building options ###

If you take a closer look at the `package.json` inside the root folder of your project, you'll see a few more advanced options

	  "gulpOptions": {
	    "root" : "./src",
	    "releasePath" : false,
	    "keepBuildPath" : false,
	    "markdownParser" : "simple"
	  },

The `root` variable specifies where to pick up source files, unless you modify the project setup, this should not need to be changed

`releasePath` can be set to a relative or absolute path. If it is set, the build process will copy the zip file to the specified location in addition to the `./dist` folder.  
You can set this for example to your Meldii library, or a backup location.

Examples:
	
	"releasePath" : "../releases",
	"releasePath" : "C:/backups",

If you set `keepBuildPath` to true, the `./build` folder will not be deleted after a build process.

If you want to use a custom markdown parser, duplicate and rename `./modules/mdbb/parsers/simple.js` and edit it as needed, then set `markdownParser` to the name of your new file. 

#### Important: ####

Both the `./build` and `./dist` folder will be cleaned before a new build!

If you want to keep a history of your files, use the `releasePath` option.

### 6. Development Tips ###

#### 1. Use a seperate Addon folder for your development projects ####

Easily switch between your standard Addon-Loadout and a clean slate for development by managing different versions of your Addon folders.

Just rename your current one and create a new, empty Addons folder, then switch between them as needed.

#### 2. Use symbolic links ####

If you prefer to keep your development files somewhere else than inside `MyDocuments/Firefall/Addons`, use a symbolic link to avoid tedious copying or sync tools.

Open a console window inside your Addons folder, and type

	mklink /J YourAddonName path\to\your\dev\files\YourAddonName\src

This directory junction will behave like an actual folder and the game will load from it.  
This works even when renaming your Addon folder, so you can still quickly swap as described above.

To get rid of the virtual folder, just delete the shortcut.

#### 3. Use the `watch` task to automatically build on file edits ####

Run 

	gulp watch

to start a process that monitors all files inside the `gulpOptions.root` folder for changes.

If a file is modified, this task automatically rebuilds your project as `gulp` would, however without the duplication of your zip file.

It is recommended to make use of the `gulpOptions.keepBuildPath` setting and symlink your `./build` path instead of `./src` for maximum convenience.

#### 4. Use fftpl to make templating even faster ####

[fftpl](https://bitbucket.org/Jotaro__/fftpl/overview) is a global node plugin that automates the setup process above even further.

Run

	npm install -g git+http://bitbucket.org/Jotaro__/fftpl.git

to install it globally, and you'll be able to create a new template simply by running

	fftpl

inside the directory you want to work in.

### 7. Advanced advanced building options ###

If you want to use your own variables in addition to those provided by the template, take a look at the `'files'` task inside the `gulpfile.js`.

Simply add the variable you want to manage to your `package.json` and edit the task instructions accordingly

	gulp.task('files', function(){
	  var filter = gulpFilter([
	    p.name+'.lua',
	    p.name+'.xml'
	  ], {restore: true});
	  
	  var files = gulp.src([
	    root + '/**/*.*'
	  ])

	  return files
		.pipe(replace(/~%yourVariableTag%~/g, p.yourVariable)) // <-- this will search and replace in ALL *.lua  and *.xml files
	    .pipe(filter)
		  .pipe(replace(/~%yourVariableTag%~/g, p.yourVariable)) // <-- this will search and replace ONLY in YourAddonName.lua and YourAddonName.xml 
	      .pipe(replace(/~%author%~/g, p.author))
	      .pipe(replace(/~%version%~/g, p.version))
	      .pipe(replace(/~%description%~/g, p.description))
	      .pipe(replace(/~%patch%~/g, p.patch))
	      .pipe(replace(/~%debug%~/g, 'false'))
	    .pipe(filter.restore)
	    .pipe(gulp.dest('./build/' + p.name))
	});

If you need more sophisticated functionality, you can copy & edit the filter and append a new one between  
 `.pipe(filter.restore)` and `.pipe(gulp.dest('./build/' + p.name))`

Example:

	gulp.task('files', function(){
	  var filter = gulpFilter([
	    p.name+'.lua',
	    p.name+'.xml'
	  ], {restore: true});

	  var customFilter = gulpFilter([
		'/**/*.txt'
	  ], {restore: true});
	  // ^ this will match any *.txt files in any folder.
	  // Look up 'glob patterns' for more information
	  
	  var files = gulp.src([
	    root + '/**/*.*'
	  ])

	  return files
	    .pipe(filter)
	      .pipe(replace(/~%author%~/g, p.author))
	      .pipe(replace(/~%version%~/g, p.version))
	      .pipe(replace(/~%description%~/g, p.description))
	      .pipe(replace(/~%patch%~/g, p.patch))
	      .pipe(replace(/~%debug%~/g, 'false'))
	    .pipe(filter.restore)
		.pipe(customFilter)
		  .pipe(replace(/~%yourVariableTag%~/g, p.yourVariable)) // <-- this will be applied to all files as defined in customFilter
		.pipe(customFilter.restore)
	    .pipe(gulp.dest('./build/' + p.name))
	});

## Issue tracking ##

Please report any issues you might find [here](https://bitbucket.org/Jotaro__/template_dev/issues).

## Changelog ##
	
	1.2.0 - shiny polish

	# Added `watch` task to immediately build all files on changes (credit: Arkii)
	# Wizard now asks for an initial version number
	# wizard now supports validating input
	# Now uses proper [list] formatting in bbcode
	# Added more styling options (underline, colored)

	# Bugfixes
		- no longer applies formatter inside [code] tags
		- reworked the task queue so they should all run in the proper sequence now
		- improved pattern matching for bold/italics
		- should no longer match formatting in urls/images
	

	1.1.0 - now with more automation
	
	# readme.md to bbcode converter
	# meldii download button generator

	# Bugfixes
		- fixed Addon template errors
		- dynamically set author and description instead of once at setup 

	1.0.0 - initial release

	# Initial basic template  
	# Basic lib stub  
	# Automated build process    
	# Automated `melder_info.ini` generator  
	# Automatically zip a Meldii-compatible file for distribution.

