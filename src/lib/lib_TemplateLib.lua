----
--  TemplateLib Library
--  @Author Jotaro
--  @Version 0.0.0
--

--[===[
  
  Usage:
    
    local example = TemplateLib.new(things);
    
    example:setFoo(false);
    
    example:getFoo();
      returns the value of foo
      
    example:printFoo()
      does stuff
  
--]===]

if TemplateLib then
  return nil
end
TemplateLib = {}

local API = {}
local OPTIONS = {
  foo = true
}


local METATABLE = {
  __index = function(t,key) return API[key]; end,
  __newindex = function(t,k,v) error("cannot write to value '"..k); end
};

function TemplateLib.new(args)
  local LIB = {
    ARGS = args,
    OPTIONS = OPTIONS
  };
  
  setmetatable(LIB, METATABLE);
  return LIB;
end

-- getters and setters

function API:setFoo(value)
  self.OPTIONS.foo = (value);
end

function API:getFoo(value)
  return self.OPTIONS.foo;
end

-- api

function API:printFoo()
  log(self.OPTIONS.foo);
end
