----
--  ~%name%~
--  @Description ~%description%~
--  @Author ~%author%~
--  @Version ~%version%~
--  @Patch ~%patch%~
--

require "lib/lib_InterfaceOptions";

require "lib/lib_Debug";

require "./lib/lib_TemplateLib";

--~~~~~~~~~~~~~--
-- Constants   --
--~~~~~~~~~~~~~--



--~~~~~~~~~~~~~--
-- Options     --
--~~~~~~~~~~~~~--

local ADDON = {
  NAME = "~%name%~",
  VERSION = "~%version%~",
  AUTHOR = "~%author%~",
  LOADED = false,
  DEBUG = '~%debug%~' -- yes this is a string
}

local OPTIONS = {
  enabled = true,
  foo = true
}

OnMessageSetData = {
  enabled = function(message)
    OPTIONS.enabled = message
    if(ADDON.LOADED and message) then
      init();
    else
      if(ADDON.LOADED) then
        shutdown();
      end
    end
  end,
  
  foo = function(message)
    OPTIONS.foo = message
    systemMessage("foo: " .. tostring(message));
  end
  
  --add more option actions here
}

InterfaceOptions.SaveVersion(1)
InterfaceOptions.StartGroup({id="enabled", label="Enable "..ADDON.NAME, checkbox=true, default=OPTIONS.enabled, tooltip="Disables the Addon if unchecked"});

  -- this section will be collapsed when disabled

  InterfaceOptions.AddCheckBox({id="foo", label="foo", checkbox=true, default=OPTIONS.foo, tooltip="does stuff"});
  
  -- use as line seperator when needed
  -- -------------------------------- 
  -- InterfaceOptions.AddMultiArt({id="LINE", texture="CheckBox_White", region="backdrop", height=1, width=600, padding=12, tint="#505050"})
  -- -------------------------------- 
  
InterfaceOptions.StopGroup()
InterfaceOptions.NotifyOnLoaded(true)

function OnMessage(msg)
  if msg.type == "__LOADED" then
    ADDON.LOADED = true
    init();
  end
  if not ( OnMessageSetData[msg.type] ) then return nil end
  OnMessageSetData[msg.type](msg.data);
end

--~~~~~~~~~~~~~--
-- Collections --
--~~~~~~~~~~~~~--


--~~~~~~~~~~~~~--
-- Events      --
--~~~~~~~~~~~~~--

function OnComponentLoad() 

  log(ADDON.NAME..": --- version " .. ADDON.VERSION ..  " ---")
  local debug = (ADDON.DEBUG == '~%debug%~' or ADDON.DEBUG == 'true')
  Debug.EnableLogging(debug);
    
  InterfaceOptions.SetCallbackFunc(function(id, val)  OnMessage({type=id, data=val})  end, ADDON.NAME.." "..ADDON.VERSION)
  
end

--~~~~~~~~~~~~~--
-- Functions   --
--~~~~~~~~~~~~~--

-- general
function init()
  if(OPTIONS.enabled)then
    systemMessage(ADDON.NAME.." "..ADDON.VERSION..' loaded!')
  end
end

function shutdown()
  -- disable any unwanted functionality
end

function systemMessage(args)
  Component.GenerateEvent("MY_SYSTEM_MESSAGE", {text=tostring(args)})
end




